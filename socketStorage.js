class SocketStorage {
    #socketIdMap = new Map(); //socket -> id
    #addrSet = new Set(); //fullAddr
    #idSocketMap = new Map(); //id -> socket

    getFullAddr(socket) {
        const {address, port} = socket.address();
        return `${address}:${port}`;
    }
    addrExists(addr) {
        return this.#addrSet.has(addr);
    }
    addSocket(socket) {
        this.#socketIdMap.set(socket, -1);
        this.#addrSet.add(this.getFullAddr(socket));
        socket.on('close', () => {
            this.remove(socket);
        });
    }
    setId(id, socket) {
        this.#idSocketMap.delete(this.#socketIdMap.get(socket));
        this.#idSocketMap.set(id, socket);
        this.#socketIdMap.set(socket, id);
    }
    getById(id) {
        return this.#idSocketMap.get(id);
    }
    remove(socket) {
        const id = this.#socketIdMap.get(socket);
        this.#idSocketMap.delete(id);
        this.#socketIdMap.delete(socket);
        this.#addrSet.delete(this.getFullAddr(socket));
    }
    forEach(cb) {
        return this.#socketIdMap.forEach(cb);
    }
}

module.exports = {
    SocketStorage
};