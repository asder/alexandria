module.exports = Object.freeze({
    udpPorts: [41000, 41100],
    broadcastIp: '255.255.255.255',
    maxDataLength: 10000,
    nodeId: Math.floor(Math.random() * 100000).toString(),
    socketHandshakeTimeout: 10000
});