const { createSocket } = require('dgram');
const { udpPorts, broadcastIp } = require('./config');
const { methods } = require('./methodHandler');

class NodeDiscover {
    #udpSocket;
    #pingCallbacks = new Set();

    async init() {
        return new Promise((resolve, reject) => {
            const tryUdpPorts = (udpPort) => {
                this.#udpSocket = createSocket('udp4');
                this.#udpSocket.on('message', (msg, {address}) => {
                    try {
                        const message = JSON.parse(msg);
                        const { method, params: [port] } = message;
                        switch(method) {
                            case methods.ping:
                                this.#pingCallbacks.forEach((cb) => {
                                    cb({address, port});
                                });
                                break;
                            default:
                                console.log(`Unknows message method ${methtod}`);
                        }
                    } catch(e) {
                        console.log(e);
                    }
                });

                this.#udpSocket.on('listening', () => {
                    this.#udpSocket.setBroadcast(true);
                    const address = this.#udpSocket.address();
                    console.log(`Server udp listening on ${address.address}:${address.port}`);
                    resolve(udpPort);
                });

                this.#udpSocket.on('error', (err) => {
                    console.log(JSON.stringify(err));
                    this.#udpSocket.close();
                    if(err.code === 'EADDRINUSE' && udpPort < udpPorts[1]) {
                        udpPort++;
                        tryUdpPorts(udpPort);
                    } else {
                        reject();
                    }
                });

                this.#udpSocket.bind(udpPort);
            };

            tryUdpPorts(udpPorts[0]);
        });
    }

    pingNodes(tcpServerPort) {
        const message = JSON.stringify({
            method: methods.ping,
            params: [tcpServerPort]
        });
        for(let udpPort=udpPorts[0]; udpPort<udpPorts[1]; udpPort++) {
            this.#udpSocket.send(message, 0, message.length, udpPort, broadcastIp);
        }
    }

    onPing(callback) {
        this.#pingCallbacks.add(callback);
        return () => {
            this.#pingCallbacks.delete(callback);
        };
    }
}

module.exports = {
    NodeDiscover
};