const { maxDataLength } = require('./config');

const JsonDataHandler = Object.freeze({
    isValidRCP: (json) => {
        return typeof json.method === 'string' && json.params instanceof Array;
    },
    isValidEncoded: (json) => {
        return typeof json.e === 'string' && typeof json.iv === 'string';
    },
    setSocketDataCallback: (callback) => {
        let data = '';
        let jsonStack = 0;
        const sliceChunkToJsonStart = (chunk) => {
            const startIndex = chunk.indexOf('{');
            return startIndex === -1 ? '' : chunk.slice(startIndex);
        };
        return (chunk) => {
            chunk = chunk.toString();
            if(data.length === 0) {
                chunk = sliceChunkToJsonStart(chunk);
            }
            for(let i=0; i<chunk.length; i++) {
                if(chunk[i] === '{') {
                    jsonStack++;
                } else if(chunk[i] === '}') {
                    jsonStack--;
                }

                if(jsonStack < 0) {
                    console.log(new Error('invalid_json'));
                    data = '';
                    jsonStack = 0;
                    chunk = sliceChunkToJsonStart(chunk.slice(i + 1));
                    i = -1;     
                } else if(jsonStack === 0) {
                    data += chunk.slice(0, i + 1);
                    try {
                        const json = JSON.parse(data);
                        callback(null, json);
                    } catch(e) {
                        console.log(e);
                    }

                    chunk = sliceChunkToJsonStart(chunk.slice(i + 1));
                    i = -1;
                    data = '';
                }
            }
            data += chunk;
            if(data.length > maxDataLength) {
                console.log(new Error('json_length_exceeded'));
                data = '';
                jsonStack = 0;
            }
        };
    }
});

module.exports = {
    JsonDataHandler
};