const { nodeId } = require('./config');

const methods = Object.freeze({
    handleMessage: 'handleMessage',
    ping: 'ping',
    assignId: 'assignId'
});
 
class MethodHandler {
    #dependencies;
    constructor(dependencies = {}) {
        this.#dependencies = dependencies;
    }

    handleMethod({jsonRCP, socket}) {
        const {socketStorage} = this.#dependencies;
        const { method, params } = jsonRCP;
        switch(method) {
            case methods.handleMessage:
                const [text, senderId, recipientId, timestamp] = params;
                console.log(`${text}\n${senderId}\n${recipientId===-1?'':recipientId+'\n'}${new Date(timestamp)}\n`);
                break;
            case methods.assignId:
                const [id] = params;
                if(socketStorage.getById(id)) {
                    if(id === nodeId) {
                        socketStorage.remove(socket);
                    } else {
                        socket.end();
                    }
                } else {
                    socketStorage.setId(id, socket);
                }
                break;
            default:
                console.log(`Unknows jsonRCP method ${method}`);
        }
    }
}

module.exports = {
    MethodHandler,
    methods
};