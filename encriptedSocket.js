const { socketHandshakeTimeout } = require('./config');

function EncriptedSocket({socket, encryption}) {
    const ESocket = function() {
        let isEncrypted;
        let stack = [];
        
        Object.assign(this, {
            isEncrypted() {
                return isEncrypted;
            },
            writeJSON(json) {
                return this.write(JSON.stringify(json));
            },
            write(data) {
                if(!isEncrypted) {
                    stack.push(data);
                    return;
                }
                ESocket.prototype.write(JSON.stringify(encryption.encrypt(data)));
            }
        })

        encryption.waitForHandshake(
            () => {
                isEncrypted = true;
                stack.forEach((data) => {
                    this.write(data);
                });
                stack = [];
            }, 
            () => {
                this.end();
            }, 
            socketHandshakeTimeout
        );

    };
    ESocket.prototype = socket;

    return new ESocket();
}

module.exports = {
    EncriptedSocket
};