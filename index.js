const net = require('net');
const readline = require('readline');
const { Endcrypt } = require('endcrypt');
const { NodeDiscover } = require('./nodeDiscover');
const { MethodHandler, methods } = require('./methodHandler');
const { nodeId } = require('./config');
const { JsonDataHandler } = require('./JsonDataHandler');
const { SocketStorage } = require('./socketStorage');
const { EncriptedSocket } = require('./encriptedSocket');

const socketStorage = new SocketStorage();
const methodHandler = new MethodHandler({socketStorage});

(async() => {
    const handleSocketConnection = (socket) => {
        const encryption = new Endcrypt();
        encryption.sendHandshake((publicKey) => {
            socket.write(JSON.stringify({publicKey}));
        });
       
        socket = new EncriptedSocket({socket, encryption});
        socketStorage.addSocket(socket);

        socket.on('data', JsonDataHandler.setSocketDataCallback((err, json) => {
           
            if(!socket.isEncrypted() && typeof json.publicKey === 'string') {
                encryption.receiveHandshake(json.publicKey);
            } else if(socket.isEncrypted() && JsonDataHandler.isValidEncoded(json)) {
                try {
                    const jsonRCP = JSON.parse(encryption.decrypt(json));
                    if(!JsonDataHandler.isValidRCP(jsonRCP)) {
                        throw new Error('invalid_json_rcp');
                    }
                    methodHandler.handleMethod({jsonRCP, socket});
                } catch(e) {
                    console.log(e);
                }
            }
        }));

        socket.on('error', (e) => {

        });

        socket.writeJSON({
            method: methods.assignId,
            params: [nodeId]
        });
    };
    
    const tcpPort = await new Promise((resolve, reject) => {
        const server = net.createServer(handleSocketConnection);

        server.on('error', (err) => {
            console.log(JSON.stringify(err));
            server.close();
            reject();
        });

        server.listen(() => { 
            const tcpPort = server.address().port;
            console.log(`Server is listening on port ${tcpPort}`);
            resolve(tcpPort);
        });
    });

    const nodeDiscover = new NodeDiscover();
    const udpPort = await nodeDiscover.init();

    nodeDiscover.onPing(({address, port}) => {
        const fullAddr = address + port;
        if(socketStorage.addrExists(fullAddr)) {
            return;
        }
        const socket = net.connect({address, port}, () => handleSocketConnection(socket));
    });

    nodeDiscover.pingNodes(tcpPort);

    console.log(`Node id: ${nodeId}`);

    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
    });
    
    rl.on('line', (line) => {
        const params = /^\s*([^\s]+)\s+(.+)\s*$/.exec(line);
        if(!params) {
            return console.log('Invalid input params');
        }
        const [l, recipientId, text] = params;
        const message = JSON.stringify({
            method: methods.handleMessage,
            params: [text, nodeId, +recipientId, Date.now()]
        });
        if(recipientId === '-1') {
            socketStorage.forEach((id, socket) => {
                socket.write(message);
            });
        } else {
            const socket = socketStorage.getById(recipientId);
            if(!socket) {
                return console.log('Recipient is disconnected or invalid id');
            }
            socket.write(message);
        }
    });
})();

